<?php

function adminlog_page_list($list = NULL) {
  $list = new redisLog;
  $items = $list->get_list($list);
  $rows = array();
  foreach ($items AS $item) {
    $account = user_load($item['uid']);
    $rows[] = array(t($item['message'], $item['variables']), l($account->name, 'user/'. $account->uid));
  }
dsm($rows);
  return theme('table', array('Message', 'User'), $rows);
}
