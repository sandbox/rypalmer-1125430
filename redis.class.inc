<?php

include dirname(__FILE__) .'/redis.php';

/**
 * Redis storage engine.
 */
class redisLog {
  var $redis;
  var $host;
  var $port;
  var $message;
  var $variables;
  
  function __construct() {
    $this->host = 'localhost';
    $this->port = '6379';
    $this->connect();
  }

  private function connect() {
    $this->redis =& new Redis($this->host, $this->port);
    if (!$this->redis->connect()) {
      return FALSE;
    }
    return TRUE;  
  }

  function set_message($message, $variables) {
    global $user;
    $this->message = $message;
    $this->variables = array_merge($variables, array('%user' => l($user->name, 'user/'. $user->uid)));
  }
  
  function set_list($list) {
    $this->list = $list;
  }
  
  function execute() {
    global $user;
    $next_log_id = $this->redis->incr('global:nextLogId');
dsm($next_log_id, 'nlid');
    $key = 'log:'. $next_log_id;
dsm($this);
dsm($key);
    dsm($this->redis->set($key .':uid', $user->uid), 'uid');
$this->connect();
    dsm($this->redis->set($key .':message', $this->message), 'message');
$this->connect();
    dsm($this->redis->set($key .':variables', $this->variables), 'vars');
$this->connect();

    // Append object ID to all relevant lists
dsm($next_log_id, 'nlid');
    dsm($this->redis->lpush('list.log', $next_log_id), 'list log');
$this->connect();
    dsm($this->redis->lpush('list.log.'. $list, $next_log_id), 'list log list');
$this->connect();
    dsm($this->redis->lpush('list.log.user.'. $user->uid, $next_log_id), 'list log user');

    // Trim the log lists
$this->connect();
    $this->redis->ltrim('list.log', 0, 10000);
$this->connect();
    $this->redis->ltrim('list.log.'. $list, 0, 1000);
  }

  function get_list($list = NULL) {
    $key = ($list) ? 'list.log.'. $list : 'list.log';
    $item_ids = $this->redis->lrange($key, 0, 19);
dsm($item_ids, 'IDs');
    $items = array();
    foreach ($item_ids AS $item) {
$this->connect();
dsm($k, 'k');
      $message = $this->redis->get('log:'. $item .':message');
$this->connect();
      $variables = $this->redis->get('log:'. $item .':variables');
dsm($message, 'message');
dsm($message, 'variables');
      $items[$item] = array(
        'message' => $message,
        'variables' => $variables,
      );
    }
    return $items;
  }
  
  function close() {
    $this->redis->disconnect();
  }
}
